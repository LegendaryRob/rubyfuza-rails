$(document).ready(function() {

  $(".nav a").on("click", function(){
     $(".nav").find(".active").removeClass("active");
     $(this).parent().addClass("active");
  });

  $(".owl-carousel").owlCarousel();

  $(document).ready(function() {

    $("#owl-example").owlCarousel({
  		navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
  			pagination: false,
      	rewindSpeed: 500
  	});

  });

  // // Let's go !
  // Countdown.init();

  $('.parallax-first').parallax("50%", 0.05);
  $('.parallax-second').parallax("50%", 0.05);
  $('.parallax-third').parallax("50%", 0.05);

	$('a[data-toggle="tab"]').on('shown', function (e) {});

	$('.cycle-slideshow-fade').cycle({
		fx: 'fade',
		timeout: 4000,
		slides: '.slide'
	});

	$('.cycle-slideshow-vertical').cycle({
		fx: 'fade',
		timeout: 8000,
		slides: '.slide',
        pager: '.cycle-vertical-pager'
	});


    $("a[href$='jpg']").fancybox();
    $("a[href$='png']").fancybox();
    $("a[href$='gif']").fancybox();

    $('.fancybox-media').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {}
        }
    });

    var options = {
        scrollwheel: false,
        mapTypeControlOptions: {
            mapTypeIds: ['Styled']
        },
        center: new google.maps.LatLng(-33.918861, 18.423300),
        zoom: 16,
        disableDefaultUI: true,
        mapTypeId: 'Styled'
    };
    var div = document.getElementById('map');
    var map = new google.maps.Map(div, options);

    new google.maps.Marker({
        position: new google.maps.LatLng(-33.918861, 18.423300),
        map: map
    });

    var styles = [
        {
            "stylers": [
                { "invert_lightness": true },
                { "saturation": -75 },
                { "hue": "#005eff" }
            ]
        }

    ];
    var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
    map.mapTypes.set('Styled', styledMapType);
});
